const express = require('express');
const body = require('body-parser');
const cookie = require('cookie-parser');
const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection')
const app = express();

app.use(express.static('public'));
app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(body.urlencoded({ extended: true }));
app.use(cookie());
app.use(session({
    secret: 'Passw0rd',
    resave: true,
    saveUninitialized: true
}));
app.use(connection(mysql, {
    host: 'localhost',
    user: 'root',
    password: 'Passw0rd',
    port: 3306,
    database: 'gitlabhw3'
}, 'single'));

const route18=require('./routes/s611998018route');
app.use('/',route18);
const s6123Route=require('./routes/s611998023Route');
app.use('/',s6123Route);
const basRoute = require('./routes/s611998002Route');
app.use('/', basRoute);
const s6023Route = require('./routes/s601998023Route');
app.use('/', s6023Route);
const loginRoute = require('./routes/homeandloginRoute');
app.use('/', loginRoute);
app.listen('8081');
