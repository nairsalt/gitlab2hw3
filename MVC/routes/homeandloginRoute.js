const express = require('express');
const router = express.Router();

const homeController = require('../controllers/homeandloginController');
const validator = require('../controllers/loginValidator');

router.get('/home',homeController.log);
router.get('/login', homeController.log);
router.post('/home', validator.loginValidator, homeController.inLogin);
router.get('/home', homeController.logcom);
router.get('/logout', homeController.logout);


module.exports = router;