const express = require('express');
const router = express.Router();
const s611998018controller = require('../controllers/s611998018controller');
const validator = require('../controllers/s611998018validator');
router.get('/s611998018',s611998018controller.list);
//router.post('/s611998018/add',s611998018controller.save);
router.post('/s611998018/add',validator.addValidator,s611998018controller.saveAdd);
router.get('/s611998018/delete/:id18',s611998018controller.delete);
router.get('/s611998018/deleteform/:id18',s611998018controller.deleteform);
router.get('/s611998018/update/:id18',s611998018controller.edit);
//router.post('/s611998018/update/:id',s611998018controller.update);
router.post('/s611998018/update/:id18',validator.editValidator,s611998018controller.updateAdd);
router.get('/s611998018/new',s611998018controller.new);
//router.get('/',controller.list);

module.exports = router;
