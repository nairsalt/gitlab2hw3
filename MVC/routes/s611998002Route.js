const express = require('express');
const router = express.Router();

const basController = require('../controllers/s611998002Controller');
const validator = require('../controllers/s611998002validator');

router.get('/bas', basController.list);
router.post('/bas/add', validator.addValidator, basController.add);
router.get('/bas/delete/:id', basController.viewdelete);
router.post('/bas/delete/:id', basController.delete);
router.get('/bas/update/:id', basController.edit);
router.post('/bas/update/:id', validator.editValidator, basController.update);
router.get('/bas/new', basController.new);

module.exports = router;