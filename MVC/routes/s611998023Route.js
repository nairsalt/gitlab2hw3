const express = require('express');
const router = express.Router();
const s6123Controller = require('../controllers/s611998023Controller');
const validator = require('../controllers/s611998023Validator');
router.get('/s611998023',s6123Controller.list);
router.post('/s611998023/add',validator.addValidator,s6123Controller.add);
router.get('/s611998023/deleteNow/:id',s6123Controller.deleteNow);
router.get('/s611998023/delete/:id',s6123Controller.delete);
router.get('/s611998023/update/:id',s6123Controller.edit);
router.post('/s611998023/update/:id',validator.editValidator,s6123Controller.update);
router.get('/s611998023/new',s6123Controller.new);

module.exports = router;  