const express=require('express');
const router=express.Router();

const s601998023Controller =require("../controllers/s601998023Controller");
const validator = require('../controllers/s601998023Validator');
router.get('/s601998023',s601998023Controller.list);
router.post('/s601998023/add',validator.addValidator,s601998023Controller.save);
router.get('/s601998023/delete/:id23',s601998023Controller.delete);
router.get('/s601998023/deleteNow/:id23',s601998023Controller.deleteNow);
router.get('/s601998023/update/:id23',s601998023Controller.edit);
router.post('/s601998023/update/:id23',validator.addValidator,s601998023Controller.update);
router.get('/s601998023/new',s601998023Controller.new);

module.exports=router;
