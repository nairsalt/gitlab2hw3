
const { check } = require('express-validator');
exports.addValidator = [
check('z601998023',"เกรดเฉลี่ยไม่ถูกต้อง!").isFloat(),
check('y601998023',"ชื่อไม่ถูกต้อง").not().isEmpty(),
check('x601998023',"ปีการศึกษาไม่ถูกต้อง").isInt(),
check('w601998023',"วันเดือนปีไม่ถูกต้อง").isDate(),
check('v601998023',"เวลาไม่ถูกต้อง").not().isEmpty()
];
