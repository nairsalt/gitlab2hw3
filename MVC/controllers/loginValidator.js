const { check } = require('express-validator');
exports.loginValidator = [check('username', "ชื่อผู้ใช้ไม่ถูกต้อง กรุณากรอกข้อมูลอีกครั้ง").not().isEmpty(),
    check('password', "รหัสผู้ใช้ไม่ถูกต้อง กรุณากรอกข้อมูลอีกครั้ง").not().isEmpty()];