const { check } = require('express-validator');

exports.addValidator = [check('z611998002', "Flot ไม่ถูกต้อง").isFloat(),
    check('y611998002', "Varchar ไม่ถูกต้อง").not().isEmpty(),
    check('x611998002', "Int ไม่ถูกต้อง").isInt(),
    check('w611998002', "Date ไม่ถูกต้อง").isDate(),
    check('v611998002', "Time ไม่ถูกต้อง").not().isEmpty()
];
exports.editValidator = [check('z611998002', "Flot ไม่ถูกต้อง").isFloat(),
    check('y611998002', "Varchar ไม่ถูกต้อง").not().isEmpty(),
    check('x611998002', "Int ไม่ถูกต้อง").isInt(),
    check('w611998002', "Date ไม่ถูกต้อง").isDate(),
    check('v611998002', "Time ไม่ถูกต้อง").not().isEmpty()
];
exports.loginValidator = [check('username', "Flot ไม่ถูกต้อง").not().isEmpty(),
    check('password', "Varchar ไม่ถูกต้อง").not().isEmpty(),
];