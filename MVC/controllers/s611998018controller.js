const s611998018controller = {};

const { validationResult } = require('express-validator');

s611998018controller.list = (req,res) => {
  req.getConnection((err,conn) => {
    conn.query('SELECT * FROM mirot611998018',(err,routes) => {
      if(err){
        res.json(err);
      }
      if(req.session.user){
        res.render('s611998018/s611998018routes',{ //หาหน้า views ต้องใส่ / ด้วยเพราะมันหนาใน view ไม่เจอ
          data:routes,session: req.session
        });
        }else {
        res.redirect('/login');
        }
        }
      );
    });
  };

s611998018controller.save =(req,res) =>{
  console.log(req.body);
  const data=req.body;
  const errors =validationResult(req);
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('INSERT INTO mirot611998018 set ?',[data],(err,routes) =>{
        if(err){
          res.json(err);
        }
        console.log(routes);
        res.redirect('/s611998018');
      });
    });
  }else {
  res.redirect('/login');
  }};

      s611998018controller.saveAdd =(req,res) =>{
      const data=req.body;
      const errors =validationResult(req);
      if(req.session.user){
        if(!errors.isEmpty()){
          req.session.errors=errors;
          req.session.success=false;
          const data=null;
          res.redirect('/s611998018/new');
        }else {
          req.session.success=true;
          req.session.topic="เพิ่มข้อมูลสำเร็จ";
          req.getConnection((err,conn) =>{
            conn.query('INSERT INTO mirot611998018 set ?',[data],(err,routes) =>{
              if(err){
                res.json(err);
              }
              console.log(routes);
              res.redirect('/s611998018');
            });
          });
        }
      }else {
      res.redirect('/login');
      }};

s611998018controller.delete =(req,res) =>{
  const {id18} =req.params;
  if(req.session.user){
    req.getConnection((err,conn) =>{
      conn.query('DELETE FROM mirot611998018 WHERE id18 = ?',[id18],(err,routes) =>{
        if(err){
          res.json(err);
        }
        console.log(routes);
        res.redirect('/s611998018');
      });
    });
  }else {
  res.redirect('/login');
  }};

s611998018controller.edit =(req,res) =>{
  const {id18} =req.params;
if(req.session.user){
  req.getConnection((err,conn) =>{
    conn.query('SELECT * FROM mirot611998018 WHERE id18 = ?',[id18],(err,routes) =>{
      if(err){
        res.json(err);
      }
      res.render('s611998018/s611998018form',{
      data:routes[0],session:req.session
    });
  });
});
}else {
res.redirect('/login');
}};

s611998018controller.deleteform =(req,res) =>{
  const {id18} =req.params;
if(req.session.user){
  req.getConnection((err,conn) =>{
    conn.query('SELECT * FROM mirot611998018 WHERE id18 = ?',[id18],(err,routes) =>{
      if(err){
        res.json(err);
      }
      res.render('s611998018/deleteform',{
      data:routes[0],
      session:req.session
    });
  });
});
}else {
res.redirect('/login');
}};

s611998018controller.update =(req,res) =>{
  const {id18} =req.params;
  const data=req.body;
if(req.session.user){
  req.getConnection((err,conn) =>{
    conn.query('UPDATE mirot611998018 SET ? WHERE id18 = ?',[data,id18],(err,routes) =>{
      if(err){
        res.json(err);
      }
      res.redirect('/s611998018');
  });
});
}else {
res.redirect('/login');
}};

s611998018controller.updateAdd = (req,res)=>{
  const {id18} =req.params;
  const data=req.body;
  const errors =validationResult(req);
if(req.session.user){
  if(!errors.isEmpty()){
    req.session.errors=errors;
    req.session.success=false;
    req.getConnection((err,conn) =>{
    conn.query('SELECT * FROM mirot611998018 WHERE id18 = ?',[id18],(err,routes) =>{
        if(err){
          res.json(err);
        }
        res.render('s611998018/s611998018form',{
        data:routes[0],session:req.session
      });
    });
    });
    }else {
    req.session.success=true;
    req.session.topic="เพิ่มข้อมูลสำเร็จ";
    req.getConnection((err,conn) =>{
    conn.query('UPDATE mirot611998018 SET ? WHERE id18 = ?',[data,id18],(err,routes) =>{
        if(err){
          res.json(err);
        }
        res.redirect('/s611998018');
    });
    });
    }
}else {
res.redirect('/login');
}};

s611998018controller.new =(req,res) =>{
  const data=null;
  if(req.session.user){
    res.render('s611998018/s611998018form',{
      data:data,session:req.session
    });
  }else {
  res.redirect('/login');
  }};

s611998018controller.back =(req,res) =>{
  const data=null;
  if(req.session.user){
    res.redirect('s611998018/s611998018form',{
      data:data,session:req.session
    });
  }else {
  res.redirect('/login');
  }};

s611998018controller.home = (req,res) => {
if(req.session.user){
  req.getConnection((err,conn) => {
    if(err){
      res.json(err);
    }
    res.render('s611998018/s611998018routes',{
      data:routes,session:req.session
    });
});
}else {
res.redirect('/login');
}};



module.exports = s611998018controller;
