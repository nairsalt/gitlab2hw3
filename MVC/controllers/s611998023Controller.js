const controller ={};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
    req.getConnection((err,conn) =>{
        conn.query('SELECT * FROM mirot611998023',(err,s6123e) =>{
            if(err){
                res.json(err);
            }
            if(req.session.user){
                res.render('s611998023/s611998023',{ //หาหน้า views ต้องใส่ / ด้วยเพราะมันหนาใน view ไม่เจอ
                  data:s6123e,session: req.session
                });
                }else {
                res.redirect('/login');
                }
                }
              );
            });
          };
controller.add = (req,res) => {
    const data=req.body;
    const errors = validationResult(req);
    if(req.session.user){
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success=false;
            res.redirect('/s611998023/new');
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลเสร็จแล้ว";
            req.getConnection((err,conn)=>{
                conn.query('INSERT INTO mirot611998023 set ?',[data],(err,s6123e)=>{
            res.redirect('/s611998023');
            });
        });
    };
    }else {
    res.redirect('/login');
    }};

controller.delete = (req,res) => {
    const { id } = req.params;
if(req.session.user){
    req.getConnection((err,conn)=>{
        conn.query('SELECT * FROM mirot611998023 WHERE id23= ?',[id],(err,s6123e)=>{
            if(err){
                res.json(err);
            }
            res.render('s611998023/s611998023Delete',{session: req.session,data:s6123e[0]});
        });
    });
}else {
res.redirect('/login');
}};

controller.deleteNow = (req,res) => {
    const { id } = req.params;
    if(req.session.user){
        req.getConnection((err,conn)=>{
            conn.query('DELETE FROM mirot611998023 WHERE id23= ?',[id],(err,s6123e)=>{
                if(err){
                    res.json(err);
                }
                console.log(s6123e);
                res.redirect('/s611998023');
            });
        });
    }else {
    res.redirect('/login');
    }};

controller.edit = (req,res) => {
    const { id } = req.params;
    if(req.session.user){
        req.getConnection((err,conn)=>{
            conn.query('SELECT * FROM mirot611998023 WHERE id23= ?',[id],(err,s6123e)=>{
                if(err){
                    res.json(err);
                }
                res.render('s611998023/s611998023Form',{session: req.session,data:s6123e[0]});
            });
        });
    }else {
    res.redirect('/login');
    }};

controller.update = (req,res) => {
    const errors = validationResult(req);
    const { id } = req.params;
    const data = req.body;
    console.log(data);
        if(req.session.user){
            if(!errors.isEmpty()){
                req.session.errors=errors;
                req.session.success=false;
                req.getConnection((err,conn)=>{
                    conn.query('SELECT * FROM mirot611998023 WHERE id23= ?',[id],(err,s6123e)=>{
                        if(err){
                            res.json(err);
                        }
                        res.render('s611998023/s611998023Form',{session: req.session,data:s6123e[0]});
                    });
                });
            }else{
                req.session.success=true;
                req.session.topic="แก้ไขข้อมูลเสร็จแล้ว";
                req.getConnection((err,conn) => {
                    conn.query('UPDATE  mirot611998023 SET ?  WHERE id23 = ?',[data,id],(err,s6123e) => {
                      if(err){
                          res.json(err);
                      }
                   res.redirect('/s611998023');
                   });
                 });
            }
        }else {
        res.redirect('/login');
        }};

controller.new = (req,res) => {
    const data = null;
        if(req.session.user){
            res.render('s611998023/s611998023Form',{session: req.session,data:data});
        }else {
        res.redirect('/login');
        }};


module.exports = controller;