
const { check } = require('express-validator');
exports.addValidator = [check('z611998018', "ทศนิยมไม่ถูกต้อง").not().isEmpty(),
                      check('y611998018',   "ตัวอักษรไม่ถูกต้อง").not().isEmpty(),
                      check('x611998018',   "ตัวเลขไม่ถูกต้อง").not().isEmpty(),
                      check('w611998018',   "วันที่ไม่ถูกต้อง").not().isEmpty(),
                      check('v611998018',   "เวลาไม่ถูกต้อง").not().isEmpty()
                    ];


exports.editValidator = [check('z611998018',"ทศนิยมไม่ถูกต้อง").not().isEmpty(),
                      check('y611998018',   "ตัวอักษรไม่ถูกต้อง").not().isEmpty(),
                      check('x611998018',   "ตัวเลขไม่ถูกต้อง").not().isEmpty(),
                      check('w611998018',   "วันที่ไม่ถูกต้อง").not().isEmpty(),
                      check('v611998018',   "เวลาไม่ถูกต้อง").not().isEmpty()
                      ];
