const { check } = require('express-validator');
//check คือ ชื่อฟังชั่นที่อยู่ใน express-validator
exports.addValidator = [
    check('z611998023',"ใส่ข้อมูล Z ไม่ถูกต้อง !").isFloat(),
    check('y611998023',"ใส่ข้อมูล Y ไม่ถูกต้อง !").not().isEmpty(),
    check('x611998023',"ใส่ข้อมูล X ไม่ถูกต้อง !").isInt(),
    check('w611998023',"ใส่ข้อมูล W ไม่ถูกต้อง !").isDate(),
    check('v611998023',"ใส่ข้อมูล V ไม่ถูกต้อง !").not().isEmpty()];
exports.editValidator = [
    check('z611998023',"ใส่ข้อมูล Z ไม่ถูกต้อง !").isFloat(),
    check('y611998023',"ใส่ข้อมูล Y ไม่ถูกต้อง !").not().isEmpty(),
    check('x611998023',"ใส่ข้อมูล X ไม่ถูกต้อง !").isInt(),
    check('w611998023',"ใส่ข้อมูล W ไม่ถูกต้อง !").isDate(),
    check('v611998023',"ใส่ข้อมูล V ไม่ถูกต้อง !").not().isEmpty()];