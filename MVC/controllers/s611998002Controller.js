const controller = {};
const { validationResult } = require('express-validator');
const { default: validator } = require('validator');
controller.list = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query("select * from mirot611998002", (err, s611998002) => {
            if (err) {
                res.json(err);
            }
            if(req.session.user){
                res.render('s611998002/s611998002',{ //หาหน้า views ต้องใส่ / ด้วยเพราะมันหนาใน view ไม่เจอ
                  data:s611998002,session: req.session
                });
                }else {
                res.redirect('/login');
                }
                }
              );
            });
          };

controller.add = (req, res) => {
    const data = req.body;
    const errors = validationResult(req);
    if(req.session.user){
        req.getConnection((err, conn) => {
            conn.query('insert into mirot611998002 set ?', [data], (err, s611998002) => {
                if (!errors.isEmpty()) {
                    req.session.errors = errors;
                    req.session.success = false;
                    res.redirect('/bas/new')
                } else {
                    req.session.success = true;
                    req.session.topic = "เพิ่มข้อมูลสำเร็จ";
                    console.log('../views/s611998002/s611998002');
                    res.redirect('/bas');
                };
            });
        });
    }else {
    res.redirect('/login');
    }};

controller.viewdelete = (req, res) => {
    const { id } = req.params;
if(req.session.user){
    req.getConnection((err, conn) => {
        conn.query('select * from mirot611998002 where id02 = ?', [id], (err, s611998002) => {
            if (err) {
                res.json(err);
            }
            res.render('../views/s611998002/s611998002FormDelete', {
                data: s611998002[0],
                session: req.session
            });
        });
    });
}else {
res.redirect('/login');
}};


controller.delete = (req, res) => {
    const { id } = req.params;
    if(req.session.user){
        req.getConnection((err, conn) => {
            conn.query('delete from mirot611998002 where id02 = ?', [id], (err, s611998002) => {
                if (err) {
                    res.json(err);
                }
                req.session.success = true;
                req.session.topic = "ลบข้อมูลสำเร็จ";
                console.log('../views/s611998002/s611998002');
                res.redirect('/bas');
            });
        });
    }else {
    res.redirect('/login');
    }};

controller.edit = (req, res) => {
    const { id } = req.params;
    if(req.session.user){
        req.getConnection((err, conn) => {
            conn.query('select * from mirot611998002 where id02 = ?', [id], (err, s611998002) => {
                if (err) {
                    res.json(err);
                }
                res.render('../views/s611998002/s611998002Form', {
                    data: s611998002[0],
                    session: req.session
                });
            });
        });
    }else {
    res.redirect('/login');
    }};

controller.update = (req, res) => {
    const { id } = req.params;
    const data = req.body;
    const errors = validationResult(req);
if(req.session.user){
    req.getConnection((err, conn) => {
        conn.query('update mirot611998002 set ? where id02 = ?', [data, id], (err, s611998002) => {
            if (!errors.isEmpty()) {
                req.session.errors = errors;
                req.session.success = false;
                conn.query('select * from mirot611998002 where id02 = ?', [id], (err, s611998002) => {
                    res.render('../views/s611998002/s611998002Form', {
                        data: s611998002[0],
                        session: req.session
                    });
                });
            } else {
                req.session.success = true;
                req.session.topic = "อัพเดทข้อมูลสำเร็จ";
                console.log('../views/s611998002/s611998002');
                res.redirect('/bas');
            };
        });
    });
}else {
res.redirect('/login');
}};


controller.new = (req, res) => {
    const data = null;
    if(req.session.user){
        res.render('../views/s611998002/s611998002Form', {
            data: data,
            session: req.session
        });
    }else {
    res.redirect('/login');
    }};

module.exports = controller;