const controller = {};
const { validationResult } = require('express-validator');
const { default: validator } = require('validator');

controller.log = (req, res) => {
    res.render('LoginForm', { session: req.session });
};
controller.login = function(req, res) {
    if (req.body.username == 'inwBoy' && req.body.password == 'Passw0rd') {
        req.session.user = req.body.username;
        res.render('home', { session: req.session });
    } else {
        res.render('LoginForm', { session: req.session });
    };
};
controller.logcom = function(req, res) {
    if (req.session.user) {
        res.render('home', { session: req.session });
    } else {
        res.render('LoginForm', { session: req.session });
    }
};

controller.inLogin =(req,res) =>{
    const errors =validationResult(req);
    if (!errors.isEmpty()) {
        req.session.errors=errors;
        req.session.success=false;
        res.render('LoginForm', { session: req.session });
      }else {
        req.session.success=true;
        if(!req.session.user){
            req.session.topic="Password ไม่ถูกต้อง";
        }if(req.body.username == 'inwBoy' && req.body.password == 'Passw0rd') {
            req.session.user = req.body.username;
            req.session.topic = "Login สำเร็จ";
            res.render('home',{ session: req.session });
        }else{
            res.render('LoginForm', { session: req.session });
        }
      }
};

controller.logout = function(req, res) {
    req.session.destroy();
    res.redirect('/login');
};


module.exports = controller;