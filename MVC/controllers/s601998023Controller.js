const controller ={};
const { validationResult } = require('express-validator');


controller.list=(req,res) => {
  req.getConnection((err,conn) =>{
    conn.query('select * from mirot601998023',(err,s601998023s) =>{
      if (err) {
        res.json(err);
      }
        if(req.session.user){
          res.render('s601998023/s601998023s',{data:s601998023s,session: req.session});
        }else {
        res.redirect('/login');
        }
      }
    );
  });
};

controller.save=(req,res)=>{
const data=req.body;
const errors = validationResult(req);
req.getConnection((err,conn)=>{
  conn.query('insert into mirot601998023 set?',[data],(err,s601998023s) =>{
    if (!errors.isEmpty()) {
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/s601998023/new')

    }else {
      req.session.success=true;
      req.session.topic='เพิ่มข้อมูลสำเร็จ';

      res.redirect('/s601998023');
    }; // save ใน customer ไปเลย
  });
});
};

controller.delete=(req,res)=>{
const { id23 }=req.params;
if(req.session.user){
  req.getConnection((err,conn)=>{
    conn.query('select * from mirot601998023 where id23= ?',[id23],(err,s601998023s) =>{
      if (err) {
        res.json(err);
      }
        res.render('../views/s601998023/deleteForm',{
        session: req.session,
        data:s601998023s[0]
      });
    });
  }); 
}else {
res.redirect('/login');
}};

controller.edit=(req,res)=>{
const { id23 }=req.params;
const errors = validationResult(req);
if(req.session.user){
  req.getConnection((err,conn)=>{
    conn.query('Select * from mirot601998023 where id23= ?',[id23],(err,s601998023s) =>{
      if (err) {
        res.json(err);
      }
      res.render('s601998023/s601998023Form',{
        session: req.session,
        data:s601998023s[0]
      });
    });
  });
}else {
res.redirect('/login');
}};

controller.update = (req,res) => {
  const {id23} = req.params;
  const data = req.body;
  const errors = validationResult(req);
      if(req.session.user){
        if(!errors.isEmpty()){
          req.session.errors=errors;
          req.session.success=false;
          req.getConnection((err,conn) =>{
            conn.query('select * from mirot601998023 where id23 = ?',[id23],(err,s601998023s) =>{
              if(err){
                res.json(err);
              }
              res.render('../views/s601998023/s601998023Form',{
                session: req.session,
                data:s601998023s[0]
              });
            });
          });
        }else {
          req.session.success=true;
          req.session.topic="แก้ไขเรียบร้อย";
          req.getConnection((err,conn) =>{
            conn.query('update mirot601998023 set ? where id23 = ?',[data,id23],(err,s601998023s)=> {
              if(err){
                res.json(err);
              }
            res.redirect('/s601998023')
            });
          });
        }
      }else {
      res.redirect('/login');
      }};
      
controller.deleteNow=(req,res)=>{
const { id23 }=req.params;
if(req.session.user){
  req.getConnection((err,conn)=>{
    conn.query('Delete from mirot601998023 where id23= ?',[id23],(err,s601998023s) =>{
      if (err) {
        res.json(err);
      }
      console.log('../views/s601998023/s601998023s');
      res.redirect('/s601998023');
    });
  });
}else {
res.redirect('/login');
}};


controller.new =(req,res) => {
  const data=null;
  if(req.session.user){
    res.render('s601998023/s601998023Form',{
      data:data,session: req.session
    });
  }else {
  res.redirect('/login');
  }};




module.exports = controller;
